﻿using System;
using System.Collections.Generic;
using SharpGL;
using SharpGL.SceneGraph;
using DV_Model;

namespace DV_View
{
    class Graphic : IGraphic
    {
        private OpenGL gl;

        public Graphic(OpenGLEventArgs _gl)
        {
            gl = _gl.OpenGL;
            gl.ClearColor(1f, 1f, 1f, 0.3f);
            gl.MatrixMode(OpenGL.GL_MODELVIEW);
        }

        public void Clear()
        {
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
        }

        public void DrawEllipse(DV_Model.Point center, double rad1, double rad2, IPen pen, IBrush fillcolor)
        {
            double split = 300;

            #region Contour
            // включаем сглаживание
            gl.Enable(OpenGL.GL_LINE_SMOOTH);
            gl.Enable(OpenGL.GL_POLYGON_SMOOTH);
            gl.Enable(OpenGL.GL_BLEND);
            gl.Hint(OpenGL.GL_LINE_SMOOTH_HINT, OpenGL.GL_NICEST);
            gl.Hint(OpenGL.GL_POLYGON_SMOOTH_HINT, OpenGL.GL_NICEST);
            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_SRC_ALPHA);

            gl.Color(pen.MainColor.R, pen.MainColor.G, pen.MainColor.B);
            gl.LineWidth(pen.width);
            gl.Begin(OpenGL.GL_LINES);
            double dfi = 4.0 * 360.0 / split;
            for (double fi = 0; fi <= 360; fi += dfi)
            {
                double rad = Math.PI * fi / 180; // перевод в радианы
                double x1 = center.X + rad1 * Math.Cos(rad);
                double y1 = center.Y + rad2 * Math.Sin(rad);

                rad = Math.PI * (fi + dfi) / 180; // перевод в радианы
                double x2 = center.X + rad1 * Math.Cos(rad);
                double y2 = center.Y + rad2 * Math.Sin(rad);

                gl.Vertex(x1, y1);
                gl.Vertex(x2, y2);
            }
            gl.End();

            // отключаем сглаживание
            gl.Disable(OpenGL.GL_LINE_SMOOTH);
            gl.Disable(OpenGL.GL_POLYGON_SMOOTH);
            gl.Disable(OpenGL.GL_BLEND);
            #endregion

            #region Filled_part
            gl.LineWidth(pen.width);
            gl.Begin(OpenGL.GL_POLYGON);
            dfi = 360.0 / split;
            for (double fi = 0; fi <= 360; fi += dfi)
            {
                double rad = Math.PI * fi / 180; // перевод в радианы
                double x = center.X + rad1 * Math.Cos(rad);
                double y = center.Y + rad2 * Math.Sin(rad);
                gl.Vertex(x, y);
            }
            gl.End();
            #endregion
        }

        public void DrawRectangle(List<Point> point_, IPen linecolor, IBrush fillcolor)
        {
            gl.Color(linecolor.MainColor.R, linecolor.MainColor.G, linecolor.MainColor.B);
            gl.LineWidth(linecolor.width);
            gl.Begin(OpenGL.GL_QUADS);
            gl.Vertex(point_[0].X, point_[0].Y);
            gl.Vertex(point_[1].X, point_[0].Y);
            gl.Vertex(point_[1].X, point_[1].Y);
            gl.Vertex(point_[0].X, point_[1].Y);
            gl.End();
        }

        public void DrawLine(DV_Model.Point p1, DV_Model.Point p2, IPen pen)
        {
            // включаем сглаживание
            gl.Enable(OpenGL.GL_LINE_SMOOTH);
            gl.Enable(OpenGL.GL_POLYGON_SMOOTH);
            gl.Enable(OpenGL.GL_BLEND);
            gl.Hint(OpenGL.GL_LINE_SMOOTH_HINT, OpenGL.GL_NICEST);
            gl.Hint(OpenGL.GL_POLYGON_SMOOTH_HINT, OpenGL.GL_NICEST);
            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_SRC_ALPHA);

            gl.Color(pen.MainColor.R, pen.MainColor.G, pen.MainColor.B);
            gl.LineWidth(pen.width);
            gl.Begin(OpenGL.GL_LINES);
            gl.Vertex(p1.X, p1.Y);
            gl.Vertex(p2.X, p2.Y);
            gl.End();

            // отключаем сглаживание
            gl.Disable(OpenGL.GL_LINE_SMOOTH);
            gl.Disable(OpenGL.GL_POLYGON_SMOOTH);
            gl.Disable(OpenGL.GL_BLEND);
        }

        public void Polygon(IEnumerable<DV_Model.Point> points, IPen pen, IBrush brush, bool is_closed)
        {
            gl.Color(brush.MainColor.R, brush.MainColor.G, brush.MainColor.B);
            gl.Begin(is_closed ? OpenGL.GL_POLYGON : OpenGL.GL_LINES);
            foreach (var point in points)
                gl.Vertex(point.X, point.Y);
            gl.End();
        }
    }
}
