﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SharpGL;
using SharpGL.SceneGraph;
using ReactiveUI;
using System.ComponentModel;
using System.Windows.Controls.Primitives;
using System.Reactive;
using System.Reactive.Disposables;
using System.Windows.Interactivity;
using DV_View;

namespace DaVinci
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IViewFor<ViewModel.ViewModel>, IReactiveObject
    {
        private Size OpenGLControl_size;

        public ReactiveCommand<string, Unit> ComandLine;
        public ReactiveCommand<Unit, Unit> UnclickType;

        public ReactiveCommand<DV_Model.IGraphic, Unit> Draw;

        public ReactiveCommand<(object, OpenGLEventArgs), Unit> CreateGraphics;

        private DV_Model.Color curPenColor;
        private float curWidth = 3;

        public MainWindow()
        {
            DataContext = this;
            InitializeComponent();

            this.WhenActivated(disposer =>
            {
                ComandLine = ReactiveCommand.Create<string, Unit>(s =>
                {
                    ViewModel.ActiveFigure = null;
                    ViewModel.ActiveType = s;
                    return default;
                }).DisposeWith(disposer);

                UnclickType = ReactiveCommand.Create<Unit, Unit>(_ =>
                {
                    ViewModel.ActiveType = null;
                    return default;
                }).DisposeWith(disposer);

                this.RaisePropertyChanged("ComandLine");
                this.RaisePropertyChanged("UnclickType");
            });

            widthField.Text = curWidth.ToString();
            clrPicker.SelectedColor = Colors.ForestGreen;
            curPenColor.R = clrPicker.SelectedColor.Value.R;
            curPenColor.G = clrPicker.SelectedColor.Value.G;
            curPenColor.B = clrPicker.SelectedColor.Value.B;
            curPenColor.A = clrPicker.SelectedColor.Value.A;
        }


        Graphic graphic;
        ViewModel.ViewModel viewModel = new ViewModel.ViewModel();
        public ViewModel.ViewModel ViewModel { get => viewModel; set { } }
        object IViewFor.ViewModel { get => ViewModel; set => ViewModel = (ViewModel.ViewModel)value; }

        public void RaisePropertyChanging(PropertyChangingEventArgs args)
        {
            PropertyChanging.Invoke(this, args);
        }

        public void RaisePropertyChanged(PropertyChangedEventArgs args)
        {
            PropertyChanged.Invoke(this, args);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public event PropertyChangingEventHandler PropertyChanging;

        private void HandleCheck(object sender, RoutedEventArgs e)
        {
            string type_ = (string)((ToggleButton)sender).Content;
            ComandLine.Execute(type_).Subscribe();
        }

        private void HandleUnchecked(object sender, RoutedEventArgs e)
        {
            UnclickType.Execute().Subscribe();
        }

        private void OpenGLControl_OpenGLInitialized(object sender, OpenGLEventArgs args)
        {
            graphic = new Graphic(args);
        }

        private void OpenGLControl_OpenGLDraw(object sender, OpenGLEventArgs args)
        {
            ViewModel.Draw.Execute(graphic).Subscribe();
        }

        private void OpenGLControl_Resized(object sender, OpenGLEventArgs args)
        {
            var gl = args.OpenGL;
            gl.LoadIdentity();
            gl.Ortho2D(0, OpenGLControl_size.Width, 0, OpenGLControl_size.Height);
        }

        private void OpenGLControl_SizeChanged(object sender, SizeChangedEventArgs size)
        {
            OpenGLControl_size = size.NewSize;
        }

        private void OpenGLControl_MouseDown(object sender, MouseEventArgs e)
        {
            var SelectedObject = e.Source as UIElement;
            Point pos = e.GetPosition(SelectedObject);
            ViewModel.MouseDown.Execute((new DV_Model.Point(pos.X, OpenGLControl_size.Height - pos.Y), new DV_Model.Pen(curPenColor, curWidth))).Subscribe();
        }

        private void OpenGLControl_MouseMove(object sender, MouseEventArgs e)
        {
            var SelectedObject = e.Source as UIElement;
            Point pos = e.GetPosition(SelectedObject);
            ViewModel.MouseMove.Execute(new DV_Model.Point(pos.X, OpenGLControl_size.Height - pos.Y)).Subscribe();
        }

        private void OpenGLControl_MouseUp(object sender, MouseButtonEventArgs e)
        {
            var SelectedObject = e.Source as UIElement;
            Point pos = e.GetPosition(SelectedObject);
            ViewModel.MouseUp.Execute(new DV_Model.Point(pos.X, OpenGLControl_size.Height - pos.Y)).Subscribe();
            

            //Тут сделать и другие клавиши
            Line.IsChecked = false;
            Ellipse.IsChecked = false;
            Rectangle.IsChecked = false;
        }
        private void clrPicker_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            curPenColor.R = clrPicker.SelectedColor.Value.R;
            curPenColor.G = clrPicker.SelectedColor.Value.G;
            curPenColor.B = clrPicker.SelectedColor.Value.B;
            curPenColor.A = clrPicker.SelectedColor.Value.A;
        }

        private void widthField_LostFocus(object sender, RoutedEventArgs e)
        {
            foreach (char c in widthField.Text)
            {
                if (!char.IsDigit(c))
                {
                    widthField.Text = curWidth.ToString();
                    return;
                }
            }

            curWidth = float.Parse(widthField.Text);
        }
    }
}
