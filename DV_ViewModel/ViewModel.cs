﻿using ReactiveUI;
using System;
using DynamicData;
using DV_Model;
using System.Reactive;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Composition;
using System.Linq;
using System.Composition.Hosting;
using System.Reactive.Linq;

namespace ViewModel
{
    public class ViewModel : ReactiveObject
    {
        class ImportClass
        {
            [ImportMany]
            public IEnumerable<Lazy<IFigureDescriptor, FigureDescriptorMetadata>> AvailableFigures { get; set; }
        }
        static ImportClass importClass;
        SourceList<IFigure> Figures = new SourceList<IFigure>();


        private string activType;
        private IFigure activeFigure;
        private Point? lastPoint;

        public IFigure ActiveFigure
        {
            get => activeFigure;
            set
            {
                this.RaiseAndSetIfChanged(ref activeFigure, value);
                param.Update(ActiveFigure != null ? (Param?)ActiveFigure.Parameters : null);
            }
        }

        public string ActiveType {
            get => activType;
            set => activType = value;
        }

        ReadOnlyObservableCollection<IFigure> allFigures;
        public ReadOnlyObservableCollection<IFigure> AllFigures => allFigures;

        ObserParam param = new ObserParam();
        public ObserParam Parametrs => param;
        public ObservableCollection<Point> Parametrs_points => param.points;
        public ObservableCollection<Color> Parametrs_color => param.color;
        public ObservableCollection<ParamElem> Parametrs_paramElems => param.paramElems;


        public IEnumerable<string> FigureTypes => importClass.AvailableFigures.Select(fig => fig.Metadata.Type);

        public int NumberOfParameters(string type)
        {
            return importClass.AvailableFigures.First(f => f.Metadata.Type == type).Value.NumberOfPoints;
        }

        public IFigure Create(Point point, IPen pen)
        {
            List<Point> ie = new List<Point>();
            for(int i = 0; i < NumberOfParameters(activType); i++) ie.Add(point);
            var fig = importClass.AvailableFigures.First(f => f.Metadata.Type == activType).Value.Create(ie, pen);
            fig.Name = activType + " " + Figures.Count.ToString();
            return fig;
        }


        public ReactiveCommand<(Point, IPen), Unit> MouseDown { get; }
        public ReactiveCommand<Point, Unit> MouseMove { get; }
        public ReactiveCommand<Point, Unit> MouseUp { get; }


        public ReactiveCommand<Point, IFigure> Select { get; }
        public ReactiveCommand<IFigure, Unit> Add { get; }
        public ReactiveCommand<IFigure, Unit> Delete { get; }
        public ReactiveCommand<Unit, Unit> Clear { get; }
        public ReactiveCommand<IGraphic, Unit> Draw { get; }
        public ReactiveCommand<(IFigure, Point), Unit> Move { get; }
        public ReactiveCommand<(IFigure, Point, double), Unit> Scale { get; }
        public ReactiveCommand<(IFigure, Point, double), Unit> Rotate { get; }

        static ViewModel()
        {
            System.Reflection.Assembly[] assemblies = { typeof(Point).Assembly };
            var conf = new ContainerConfiguration();
            try
            {
                conf = conf.WithAssemblies(assemblies);
            }
            catch (Exception) { }

            var cont = conf.CreateContainer();
            importClass = new ImportClass();
            cont.SatisfyImports(importClass);
        }

        public ViewModel()
        {
            Figures.Connect().Bind(out allFigures).Subscribe();
            lastPoint = null;

            #region Mouse

            MouseDown = ReactiveCommand.Create<(Point, IPen), Unit>(arg =>
            {
                if(activType != null)
                {
                    ActiveFigure = Create(arg.Item1, arg.Item2);
                    Add.Execute(ActiveFigure).Subscribe();
                    param.Update(ActiveFigure != null ? (Param?)ActiveFigure.Parameters : null);
                    return default;
                }
                else 
                {
                    Select.Execute(arg.Item1).Subscribe();
                    lastPoint = arg.Item1;
                    return default;
                }
            });

            MouseMove = ReactiveCommand.Create<Point, Unit>(point =>
            {
                if (ActiveType != null && ActiveFigure != null)
                {
                    ActiveFigure.MoveLastPoint(point);
                    param.Update(ActiveFigure != null ? (Param?)ActiveFigure.Parameters : null);
                    return default;
                }
                if (ActiveType == null && lastPoint != null && ActiveFigure != null)
                {
                    ActiveFigure.Move(point - (Point)lastPoint);
                    param.Update(ActiveFigure != null ? (Param?)ActiveFigure.Parameters : null);
                    lastPoint = point;
                    return default;
                }
                return default;
            });

            MouseUp = ReactiveCommand.Create<Point, Unit>(point =>
            {
                if(ActiveType != null && ActiveFigure != null)
                {
                    ActiveFigure.MoveLastPoint(point);
                    param.Update(ActiveFigure != null ? (Param?)ActiveFigure.Parameters : null);
                    ActiveType = null;
                    ActiveFigure = null;
                }
                if (ActiveType == null && lastPoint != null && ActiveFigure != null)
                {
                    ActiveFigure.Move(point - (Point)lastPoint);
                    param.Update(ActiveFigure != null ? (Param?)ActiveFigure.Parameters : null);
                    lastPoint = null;
                }
                return default;
            });


            #endregion

            #region Standart(Selectm Clear, Add, Delete)
            Select = ReactiveCommand.Create<Point, IFigure>(
                point =>
                {
                    foreach (var fig in Figures.Items)
                    {
                        if (fig.IsContain(point))
                        {
                            ActiveFigure = fig;
                            return fig;
                        }
                    }
                    ActiveFigure = null;
                    return null;
                });

            Clear = ReactiveCommand.Create(
                () => {
                    Figures.Clear();
                });

            Add = ReactiveCommand.Create<IFigure, Unit>(
                fig =>
                {
                    Figures.Add(fig);
                    return default;
                });
            Delete = ReactiveCommand.Create<IFigure, Unit>(
                fig =>
                {
                    Figures.Remove(fig);
                    return default;
                }, Figures.CountChanged.Select(i => i > 0));
            #endregion

            Draw = ReactiveCommand.Create<IGraphic, Unit>(graphic =>
            {
                graphic.Clear();
                foreach (var figure in Figures.Items)
                    figure.Draw(graphic);
                return default;
            });

            #region MonipulWithFigure
            Move = ReactiveCommand.Create<(IFigure, Point), Unit>(
                params_ =>
                {
                    var (fig, point) = params_;
                    fig.Move(point);
                    return default;
                });
            Scale = ReactiveCommand.Create<(IFigure, Point, double), Unit>(
                params_ =>
                {
                    var (fig, point, param) = params_;
                    fig.Scale(point, param);
                    return default;
                });
            Rotate = ReactiveCommand.Create<(IFigure, Point, double), Unit>(
                params_ =>
                {
                    var (fig, point, angle) = params_;
                    fig.Rotate(point, angle);
                    return default;
                });
            #endregion

            activType = null;
        }
    }

    public class ObserParam
    {
        public ObservableCollection<Point> points = new ObservableCollection<Point>();
        public ObservableCollection<Color> color = new ObservableCollection<Color>();
        public ObservableCollection<ParamElem> paramElems = new ObservableCollection<ParamElem>();

        public void Update(Param? param = null)
        {
            points.Clear();
            color.Clear();
            paramElems.Clear();
            if (param != null)
            {
                if (param.Value.points != null)
                    points.Add(param.Value.points);
                color.Add(param.Value.color);
                if (param.Value.other != null)
                    paramElems.Add(param.Value.other);
            }

        }
    }

}