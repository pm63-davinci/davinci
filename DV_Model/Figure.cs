﻿using System;
using System.Collections.Generic;

namespace DV_Model
{
    public struct Color
    {
        public byte R, G, B, A;
        public Color(byte _R = 0, byte _G = 0, byte _B = 0, byte _A = 1)
        {
            R = _R;
            G = _G;
            B = _B;
            A = _A;
        }

        public override string ToString()
        {
            return "R: " + R.ToString() + " G: " + G.ToString() + " B: " + B.ToString();
        }

    }

    public struct ParamElem
    {
        string name;
        string param; //не знаю как сделать
        public ParamElem(string _name, string _param)
        {
            name = _name;
            param = _param;
        }
    }

    public struct Param
    {
        public IEnumerable<Point> points { get; }
        public Color color { get; }
        public IEnumerable<ParamElem> other { get; }

        public Param(IEnumerable<Point> _points,
                      Color _color,
                        IEnumerable<ParamElem> _other = null)
        {
            points = _points;
            color = _color;
            other = _other;
        }
    }


    public interface IBrush
    {
        Color MainColor { get; set; }
    }

    public interface IPen
    {
        float width { get; set; }
        Color MainColor { get; set; }
    }

    public interface IGraphic
    {
        void DrawLine(Point p1, Point p2, IPen pen);
        void DrawEllipse(Point center, double rad1, double rad2, IPen linecolor, IBrush fillcolor);
        void DrawRectangle(List<Point> point_, IPen linecolor, IBrush fillcolor);
        void Polygon(IEnumerable<Point> points, IPen pen, IBrush brush, bool is_closed);
        void Clear();
    }

    public struct Point
    {
        public double X;
        public double Y;

        public Point(double x = 0, double y = 0)
        {
            X = x; Y = y;
        }

        public static Point operator +(Point A, Point B)
        => new Point(A.X + B.X, A.Y + B.Y);

        public static Point operator -(Point A, Point B)
        => new Point(A.X - B.X, A.Y - B.Y);

        public static Point operator *(Point A, double k)
        => new Point(A.X * k, A.Y * k);

        public static Point operator *(double k, Point A)
        => new Point(A.X * k, A.Y * k);

        public override string ToString()
        {
            return "x: " + ((int)X).ToString() + " y: " + ((int)Y).ToString();
        }
    }

    public interface IFigure
    {
        string Type { get; }
        string Name { get; set; }
        IPen Pen { get; set; }
        IBrush Brush { get; set; }
        Param Parameters { get; }
        object this[string name] { get; set; }
        void Draw(IGraphic graphic);
        double Z { get; set; }
        void Move(Point vector);
        void Scale(Point center, double param);
        void Rotate(Point center, double angle);
        Point Center { get; }
        bool IsContain(Point p);
        (Point min, Point max) Gabarits { get; }
        void AddPoint(Point point);
        void MoveLastPoint(Point point);
        void DeletePoint();
    }

    public interface IFigureDescriptor
    {
        string Type { get; }
        int NumberOfPoints { get; }
        IFigure Create(IEnumerable<Point> vertex, IPen pen);
    }

    public class FigureDescriptorMetadata
    {
        public string Type { get; set; }
    }
}
