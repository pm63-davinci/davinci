﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DV_Model
{
    public class Pen : IPen
    {
        private Color _color;
        private float _width;
        public Pen(Color color, float width)
        {
            this._color = color;
            this._width = width;
        }

        public float width
        {
            get => this._width;
            set => this._width = value;
        }

        public Color MainColor
        {
            get => this._color;
            set => this._color = value;
        }
    }
}
