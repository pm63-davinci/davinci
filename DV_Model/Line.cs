﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Composition;
using System.Linq;

namespace DV_Model
{
    class PolyLine : IFigure
    {
        private IPen pen;
        private IBrush brush;
        private List<Point> vertices;
        double eps = 5.0;

        public PolyLine(List<Point> vertex, IPen _pen)
        {
            vertices = vertex;
            pen = _pen;
            param = new Param(vertex, pen.MainColor);

        }

        public object this[string name]
        {
            get => throw new NotImplementedException();
            set => throw new NotImplementedException();
        }

        public string Type => "PolyLine";

        public IPen Pen
        {
            get => this.pen;
            set => this.pen = value;
        }

        public IBrush Brush
        {
            get => this.brush;
            set => this.brush = value;
        }

        Param param;
        public Param Parameters => param;


        public double Z
        {
            get => throw new NotImplementedException();
            set => throw new NotImplementedException();
        }

        public Point Center => throw new NotImplementedException();

        public (Point min, Point max) Gabarits => throw new NotImplementedException();

        public string Name { get; set; }

        public void AddPoint(Point point)
        {
            vertices.Add(point);
        }
        public void MoveLastPoint(Point point)
        {

            vertices.RemoveAt(vertices.Count - 1);
            vertices.Add(point);
        }

        public void DeletePoint()
        {
            vertices.RemoveAt(vertices.Count - 1);
        }


        public void Draw(IGraphic graphic)
        {

            for (int i = 1; i < vertices.Count; i++)
                graphic.DrawLine(vertices[i - 1], vertices[i], pen);

            //Если замкнуто
            //рисуем 0  count-1
        }

        public bool IsContain(Point p)
        {
            for (int i = 1; i < vertices.Count; i++)
            {
                var A = vertices[i - 1];
                var B = vertices[i];

                if (A.X == B.X && Math.Abs(A.X - p.X) <= eps)
                {
                    // вертикальная линия
                    if (!((A.Y <= p.Y && p.Y <= B.Y) || (B.Y <= p.Y && p.Y <= A.Y)))
                        return false;
                }
                else if (A.Y == B.Y && Math.Abs(A.Y - p.Y) <= eps)
                {
                    // горизонтальная линия
                    if (!((A.X <= p.X && p.X <= B.X) || (B.X <= p.X && p.X <= A.X)))
                        return false;
                }
                else
                {
                    // вычисляем уравнение прямой на плоскости
                    var k = (B.Y - A.Y) / (B.X - A.X);
                    var b = A.Y - k * A.X;

                    if (!(Math.Abs(k * p.X + b - p.Y) <= eps))
                        return false;
                }
            }

            return true;
        }

        public void Move(Point vector)
        {
            for (int i = 0; i < vertices.Count; i++)
                vertices[i] += vector;
        }

        public void Rotate(Point center, double angle)
        {
            var s = Math.Sin(angle);
            var c = Math.Cos(angle);

            for (int i = 0; i < vertices.Count; i++)
            {
                var NewPoint = new Point();
                var T = vertices[i] - center;
                NewPoint.X = c * T.X - s * T.Y;
                NewPoint.Y = s * T.X + c * T.Y;
                vertices[i] = NewPoint + center;
            }
        }

        public void Scale(Point center, double param)
        {
            for (int i = 0; i < vertices.Count; i++)
            {
                var vector = vertices[i] - center;
                vertices[i] = center + vector * param;
            }
        }

        public override string ToString()
        {
            return Name;
        }

    }

    [Export(typeof(IFigureDescriptor))]
    [ExportMetadata("Type", "Line")]
    public class LineDescriptor : IFigureDescriptor
    {
        public string Type => "Line";
        public int NumberOfPoints => 2;

        public IFigure Create(IEnumerable<Point> vertex, IPen pen)
        {
            var points = vertex.ToArray();
            return new PolyLine(new List<Point>(points), pen);
        }
    }
}
