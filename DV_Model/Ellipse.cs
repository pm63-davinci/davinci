﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Composition;
using System.Linq;

namespace DV_Model
{
    class Ellipse : IFigure
    {
        /// <summary>
        /// предполагается что в vertices в 0 позиции хранится центр элиипса
        /// после другие две точки которые описывают два радиуса
        /// </summary>
        private IPen pen;
        private IBrush brush;
        private List<Point> vertices;
        private List<Point> gabarits;
        //double eps = 5.0;

        public Ellipse(List<Point> vertex_, IPen pen_)
        {
            vertices = vertex_;
            gabarits = CalcGabarits();
            pen = pen_;
            param = new Param(vertices, pen.MainColor);
            //new List<ParamElem>() { new ParamElem("a", Math.Abs(vertices[1].Y - vertices[0].Y).ToString()),
            //                        new ParamElem("b", Math.Abs(vertices[2].X - vertices[0].X).ToString())});

        }

        public object this[string name]
        {
            get => throw new NotImplementedException();
            set => throw new NotImplementedException();
        }

        public string Type => "Ellipse";

        public string Name { get; set; }

        public IPen Pen
        {
            get => this.pen;
            set => this.pen = value;
        }

        public IBrush Brush
        {
            get => this.brush;
            set => this.brush = value;
        }

        Param param;
        public Param Parameters => param;

        public double Z
        {
            get => throw new NotImplementedException();
            set => throw new NotImplementedException();
        }

        public Point Center => vertices[0];

        public double CalcLengthOfVector(Point B, Point A)
        {
            return Math.Sqrt(Math.Pow((B.X - A.X), 2) + Math.Pow((B.Y - A.Y), 2));
        }
        public (Point min, Point max) Gabarits => (gabarits[0], gabarits[1]);
        public List<Point> CalcGabarits()
        {
            double a = Math.Abs(vertices[1].Y - vertices[0].Y);
            double b = Math.Abs(vertices[2].X - vertices[0].X);
            return new List<Point>(){ new Point(vertices[0].X - b, vertices[0].Y - a),
                            new Point(vertices[0].X + b, vertices[0].Y + a)};
        }

        public void AddPoint(Point point)
        {
            vertices.Add(point);
        }

        public void DeletePoint()
        {
            vertices.RemoveAt(vertices.Count - 1);
        }

        public void Draw(IGraphic graphic)
        {
            double radius1 = Math.Abs(vertices[2].X - vertices[0].X);
            double radius2 = Math.Abs(vertices[1].Y - vertices[0].Y);
            graphic.DrawEllipse(vertices[0], radius1, radius2, pen, brush);
        }

        public bool IsContain(Point p)
        {
            if (p.X > this.gabarits[0].X && p.X < this.gabarits[1].X
                && p.Y > this.gabarits[0].Y && p.Y < this.gabarits[1].Y)
                return true;
            else
                return false;
        }

        public void Move(Point vector)
        {
            for (int i = 0; i < vertices.Count; i++)
                vertices[i] += vector;
            gabarits = CalcGabarits();
        }

        public void MoveLastPoint(Point point)
        {
            var b = vertices[2].X - vertices[0].X;
            var a = vertices[1].Y - vertices[0].Y;
            var dx = point - vertices[0] - new Point(b / 2.0, a / 2.0);
            vertices[0] += dx * 0.5;
            vertices[1] = vertices[0] + new Point(0, a) + dx * 0.5;
            vertices[2] = vertices[0] + new Point(b, 0) + dx * 0.5;

            gabarits = CalcGabarits();
        }

        public void Rotate(Point center, double angle)
        {
            var s = Math.Sin(angle);
            var c = Math.Cos(angle);

            for (int i = 1; i < vertices.Count; i++)
            {
                var NewPoint = new Point();
                var T = vertices[i] - vertices[0];
                NewPoint.X = c * T.X - s * T.Y;
                NewPoint.Y = s * T.X + c * T.Y;
                vertices[i] = NewPoint + vertices[0];
            }
        }

        public void Scale(Point center, double param)
        {
            for (int i = 1; i < vertices.Count; i++)
            {
                var vector = vertices[i] - vertices[0];
                vertices[i] = vertices[0] + vector * param;
            }
        }

        public override string ToString()
        {
            return Name;
        }
    }

    [Export(typeof(IFigureDescriptor))]
    [ExportMetadata("Type", "Ellipse")]
    public class EllipseDescriptor : IFigureDescriptor
    {
        public string Type => "Ellipse";
        public int NumberOfPoints => 3;

        public IFigure Create(IEnumerable<Point> vertex, IPen pen)
        {
            var points = vertex.ToArray();
            return new Ellipse(new List<Point>(points), pen);
        }
    }
}
