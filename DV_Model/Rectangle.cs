﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Composition;
using System.Linq;

namespace DV_Model
{
    class Rectangle : IFigure
    {
        private IPen pen;
        private IBrush brush;
        private List<Point> vertices;

        public Rectangle(List<Point> vertex_, IPen pen_)
        {
            vertices = vertex_;
            pen = pen_;
            param = new Param(vertices, pen.MainColor);
        }

        object IFigure.this[string name]
        {
            get => throw new NotImplementedException();
            set => throw new NotImplementedException();
        }

        string IFigure.Type => "Rectangle";

        public string Name{ get; set; }

        public IPen Pen
        {
            get => this.pen;
            set => this.pen = value;
        }

        public IBrush Brush
        {
            get => this.brush;
            set => this.brush = value;
        }

        Param param;
        public Param Parameters => param;

        double IFigure.Z
        {
            get => throw new NotImplementedException();
            set => throw new NotImplementedException();
        }

        Point IFigure.Center => CalcCenter();

        public Point CalcCenter()
        {
            double x = (this.vertices[0].X + this.vertices[1].X) / 2;
            double y = (this.vertices[0].Y + this.vertices[1].Y) / 2;
            return new Point(x, y);
        }

        (Point min, Point max) IFigure.Gabarits => (this.vertices[0], this.vertices[1]);

        void IFigure.AddPoint(Point point)
        {
            vertices.Add(point);
        }

        void IFigure.DeletePoint()
        {
            vertices.RemoveAt(vertices.Count - 1);
        }

        void IFigure.Draw(IGraphic graphic)
        {
            graphic.DrawRectangle(vertices, pen, brush);
        }

        bool IFigure.IsContain(Point p)
        {
            if (p.X > Math.Min(this.vertices[0].X, this.vertices[1].X) && p.X < Math.Max(this.vertices[1].X, this.vertices[0].X)
                && p.Y > Math.Min(this.vertices[0].Y, this.vertices[1].Y) && p.Y < Math.Max(this.vertices[1].Y, this.vertices[0].Y))
                return true;
            else
                return false;
        }

        void IFigure.Move(Point vector)
        {
            for (int i = 0; i < vertices.Count; i++)
                vertices[i] += vector;
        }

        void IFigure.MoveLastPoint(Point point)
        {
            vertices[1] = point;            
        }

        void IFigure.Rotate(Point center, double angle)
        {
            var s = Math.Sin(angle);
            var c = Math.Cos(angle);

            for (int i = 0; i < vertices.Count; i++)
            {
                var NewPoint = new Point();
                var T = vertices[i] - center;
                NewPoint.X = c * T.X - s * T.Y;
                NewPoint.Y = s * T.X + c * T.Y;
                vertices[i] = NewPoint + center;
            }
        }

        void IFigure.Scale(Point center, double param)
        {
            for (int i = 0; i < vertices.Count; i++)
            {
                var vector = vertices[i] - center;
                vertices[i] = center + vector * param;
            }
        }

        public override string ToString()
        {
            return Name;
        }
    }

    [Export(typeof(IFigureDescriptor))]
    [ExportMetadata("Type", "Rectangle")]
    public class RectangleDescriptor : IFigureDescriptor
    {
        public string Type => "Rectangle";
        public int NumberOfPoints => 2;

        public IFigure Create(IEnumerable<Point> vertex, IPen pen)
        {
            var points = vertex.ToArray();
            return new Rectangle(new List<Point>(points), pen);
        }
    }
}
